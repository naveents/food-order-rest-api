<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210427094154 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE item_category (id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', category_name VARCHAR(150) NOT NULL, sort_order SMALLINT DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE item_category_menu_item (item_category_id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', menu_item_id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', INDEX IDX_44676026F22EC5D4 (item_category_id), INDEX IDX_446760269AB44FE0 (menu_item_id), PRIMARY KEY(item_category_id, menu_item_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE item_category_menu_item ADD CONSTRAINT FK_44676026F22EC5D4 FOREIGN KEY (item_category_id) REFERENCES item_category (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE item_category_menu_item ADD CONSTRAINT FK_446760269AB44FE0 FOREIGN KEY (menu_item_id) REFERENCES menu_item (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE item_category_menu_item DROP FOREIGN KEY FK_44676026F22EC5D4');
        $this->addSql('DROP TABLE item_category');
        $this->addSql('DROP TABLE item_category_menu_item');
    }
}
