<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210428054251 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE menu_item ADD item_image_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE menu_item ADD CONSTRAINT FK_D754D55051A8EDA6 FOREIGN KEY (item_image_id) REFERENCES item_image (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_D754D55051A8EDA6 ON menu_item (item_image_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE menu_item DROP FOREIGN KEY FK_D754D55051A8EDA6');
        $this->addSql('DROP INDEX UNIQ_D754D55051A8EDA6 ON menu_item');
        $this->addSql('ALTER TABLE menu_item DROP item_image_id');
    }
}
