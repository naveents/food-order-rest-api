<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210428091440 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $roles = json_encode([]);
        $this->addSql('INSERT INTO user (email, roles, password, username) VALUES ("admin@staytus.com", "'.$roles.'", "$argon2id$v=19$m=65536,t=4,p=1$UUJyTU41d01QS3RZQ0QyNQ$hIl1e/+xKJtL7P8fbD5WRvglxFkX9yiBLa5T/reJZj8", "admin" )');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DELETE FROM user WHERE username="admin"');
    }
}
