# Food Order Rest API

Using Symfony and Symfony API Platform to develop CRUD REST API's for Listing Food Menu, Menu Items, Categories & Menu Images.

## Installation


```bash
- Checkout repo to your local <www/htdocs> folder
  <git clone git@gitlab.com:naveents/food-order-rest-api.git>

- Change directory to food-order-rest-api

- Run composer install

- Run migrations: php bin/console doctrine:migrations:migrate

- Generate private & public key for API authentication
  openssl genpkey -out config/jwt/private.pem -aes256 -algorithm rsa -pkeyopt rsa_keygen_bits:4096
  openssl pkey -in config/jwt/private.pem -out config/jwt/public.pem -pubout

- Run your local server using symfony or php -S <your host>:8000
```
## Usage

```bash
Import post man collection.
All routes are authenticated using a Bearer Token.
```
## Sample Endpoints
```json5
Login Url: http://<your host>/api/authenticate
Json Payload
{
    "username" : "admin",
    "password": "password"
}

1. List All Menus
   URL - http://<your host>/api/menus
   Method: GET
   Authorization Type: Bearer Token

2. List All Menu Items for a Menu
   Method: GET
   URL - http://<your host>/api/menus/<menuId>/items
   Authorization Type: Bearer Token
   Response Payload:
   {
       "id": "32c95417-2b10-4863-b095-77d1706d4b66",
       "title": "Yummy Dinner",
       "menuItems": [
           {
               "id": "208342d9-1cae-4b2a-a927-5470520f8850",
               "name": "Beef Fried Rice",
               "description": "Tomato | Fried Rice | Beef | Chilly",
               "sortOrder": null,
               "itemImage": null
           },
           {
               "id": "7fe10cb3-220f-4746-b8d1-ea97a58317be",
               "name": "Chicken Fried Rice",
               "description": "Tomato | Fried Rice | Chicken | Chilly | Gralic Paste",
               "sortOrder": 5,
               "itemImage": {
                   "url": "/media/60892d472c556925220453.jpeg"
               }
           }
       ]
   }

3. Add New Category
   Method: POST
   URL - http://<your host>/api/item_categories
   Authorization Type: Bearer Token
   Request Payload:
   {
       "categoryName": "Best Sellers",
       "sortOrder": 4
   }

4. Add New Menu Item
   Method: POST
   URL - http://<your host>/api/menu_items
   Authorization Type: Bearer Token
   Request Payload: 
   {
       "name": "Beef Fried Rice",
       "description": "Tomato | Rice | Beef | Chilly",
       "menu": "/api/menus/32c95417-2b10-4863-b095-77d1706d4b66",
       "itemCategories": [
           "/api/item_categories/cc74edf5-aa3a-4864-8ac0-3aab3f027a8a"
       ],
       "sortOrder": 6
   }

5. Edit Menu Item
   Method: PUT
   URL - http://<your host>/api/menu_items/<menu_item_id>
   Authorization Type: Bearer Token
   Request Payload:
    {
        "name": "Chicken Fried Rice",
        "description": "Tomato | Fried Rice | Chicken | Chilly | Gralic Paste",
        "menu": "/api/menus/32c95417-2b10-4863-b095-77d1706d4b66",
        "itemCategories": [
            "/api/item_categories/cc74edf5-aa3a-4864-8ac0-3aab3f027a8a"
        ],
        "sortOrder": 5,
        "itemImage": "/api/item_images/9"
    }
```