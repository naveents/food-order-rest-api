<?php

namespace App\FoodSaas\Domain\Model;

use ApiPlatform\Core\Annotation\ApiResource;
use App\FoodSaas\Domain\Model\ItemCategory;
use App\FoodSaas\Domain\Model\Menu;
use App\FoodSaas\Domain\Model\Repository\MenuItemRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Ramsey\Uuid\Doctrine\UuidGenerator;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource(
 *     attributes={"order"={"sortOrder": "ASC"}},
 *     normalizationContext={"groups"={"read_menu_item"}},
 *     denormalizationContext={"groups"={"write_menu_item"}},
 *     collectionOperations={
 *          "post"
 *     },
 *     itemOperations={
 *          "get",
 *          "put",
 *          "delete"
 *      }
 * )
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity(repositoryClass=MenuItemRepository::class)
 */
class MenuItem
{
    /**
     * @ORM\Id
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=UuidGenerator::class)
     * @Groups({"menu:items_list", "read_menu_item"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=200)
     * @Groups({"menu:items_list", "read_menu_item", "write_menu_item"})
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @ORM\Column(type="text")
     * @Groups({"menu:items_list", "read_menu_item", "write_menu_item"})
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity=Menu::class, inversedBy="menuItems")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"write_menu_item"})
     */
    private $menu;

    /**
     * @ORM\ManyToMany(targetEntity=ItemCategory::class, mappedBy="menuItem")
     * @Groups({"write_menu_item", "read_menu_item"})
     */
    private $itemCategories;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     * @Groups({"menu:items_list", "read_menu_item", "write_menu_item"})
     */
    private $sortOrder;

    /**
     * @ORM\OneToOne(targetEntity=ItemImage::class, inversedBy="menuItem", cascade={"persist", "remove"})
     * @Groups({"menu:items_list", "read_menu_item", "write_menu_item"})
     */
    private $itemImage;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    public function __construct()
    {
        $this->itemCategories = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getMenu(): ?Menu
    {
        return $this->menu;
    }

    public function setMenu(?Menu $menu): self
    {
        $this->menu = $menu;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getSortOrder(): ?int
    {
        return $this->sortOrder;
    }

    public function setSortOrder(?int $sortOrder): self
    {
        $this->sortOrder = $sortOrder;

        return $this;
    }

    /**
     * @ORM\PrePersist
     */
    public function prePersist()
    {
        $this->createdAt = new \DateTimeImmutable();
        $this->updatedAt = new \DateTimeImmutable();
    }

    /**
     * @ORM\PreUpdate
     */
    public function preUpdate()
    {
        $this->updatedAt = new \DateTimeImmutable();
    }

    /**
     * @return Collection|ItemCategory[]
     */
    public function getItemCategories(): Collection
    {
        return $this->itemCategories;
    }

    public function addItemCategory(ItemCategory $itemCategory): self
    {
        if (!$this->itemCategories->contains($itemCategory)) {
            $this->itemCategories[] = $itemCategory;
            $itemCategory->addMenuItem($this);
        }

        return $this;
    }

    public function removeItemCategory(ItemCategory $itemCategory): self
    {
        if ($this->itemCategories->removeElement($itemCategory)) {
            $itemCategory->removeMenuItem($this);
        }

        return $this;
    }

    public function getItemImage(): ?ItemImage
    {
        return $this->itemImage;
    }

    public function setItemImage(?ItemImage $itemImage): self
    {
        $this->itemImage = $itemImage;
        return $this;
    }

}
