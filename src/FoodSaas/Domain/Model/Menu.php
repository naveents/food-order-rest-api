<?php

namespace App\FoodSaas\Domain\Model;

use ApiPlatform\Core\Annotation\ApiResource;
use App\FoodSaas\Domain\Model\MenuItem;
use App\FoodSaas\Domain\Model\Repository\MenuRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Doctrine\UuidGenerator;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=MenuRepository::class)
 * @ORM\HasLifecycleCallbacks()
 * @ApiResource(
 *     attributes={"order"={"sortOrder": "ASC"}},
 *     normalizationContext={"groups"={"read_menu"}},
 *     denormalizationContext={"groups"={"write_menu"}},
 *     collectionOperations={
 *          "get",
 *          "post"
 *     },
 *     itemOperations={
 *           "get",
 *           "get_menu_items"={
 *                  "method"="GET",
 *                  "path"="/menus/{id}/items",
 *                  "normalization_context"={"groups"={"menu:items_list"}}
 *            },
 *           "put",
 *           "delete"
 *     }
 * )
 */
class Menu
{
    /**
     * @ORM\Id
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=UuidGenerator::class)
     * @Groups({"read_menu", "menu:items_list", "write_menu_item"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=200)
     * @Assert\NotBlank()
     * @Groups({"read_menu", "write_menu", "menu:items_list"})
     */
    private $title;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     * @Groups({"read_menu", "write_menu"})
     */
    private $sortOrder;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"read_menu"})
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"read_menu"})
     */
    private $updatedAt;

    /**
     * @ORM\OneToMany(targetEntity=MenuItem::class, mappedBy="menu", orphanRemoval=true)
     * @Groups({"menu:items_list"})
     */
    private $menuItems;

    public function __construct()
    {
        $this->menuItems = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getSortOrder(): ?int
    {
        return $this->sortOrder;
    }

    public function setSortOrder(?int $sortOrder): self
    {
        $this->sortOrder = $sortOrder;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @ORM\PrePersist
     */
    public function prePersist()
    {
        $this->createdAt = new \DateTimeImmutable();
        $this->updatedAt = new \DateTimeImmutable();
    }

    /**
     * @ORM\PreUpdate
     */
    public function preUpdate()
    {
        $this->updatedAt = new \DateTimeImmutable();
    }

    /**
     * @return Collection|MenuItem[]
     */
    public function getMenuItems(): Collection
    {
        return $this->menuItems;
    }

    public function addMenuItem(MenuItem $menuItem): self
    {
        if (!$this->menuItems->contains($menuItem)) {
            $this->menuItems[] = $menuItem;
            $menuItem->setMenu($this);
        }

        return $this;
    }

    public function removeMenuItem(MenuItem $menuItem): self
    {
        if ($this->menuItems->removeElement($menuItem)) {
            // set the owning side to null (unless already changed)
            if ($menuItem->getMenu() === $this) {
                $menuItem->setMenu(null);
            }
        }

        return $this;
    }
}
