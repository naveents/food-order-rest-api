<?php

namespace App\FoodSaas\Domain\Model;

use ApiPlatform\Core\Annotation\ApiResource;
use App\FoodSaas\Domain\Model\MenuItem;
use App\FoodSaas\Domain\Model\Repository\ItemCategoryRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Ramsey\Uuid\Doctrine\UuidGenerator;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ApiResource(
 *     attributes={"order"={"sortOrder": "ASC"}},
 *     normalizationContext={"groups"={"read_category"}},
 *     denormalizationContext={"groups"={"write_category"}},
 *     collectionOperations={"get", "post"},
 *     itemOperations={"get", "put", "delete"}
 * )
 * @ORM\Entity(repositoryClass=ItemCategoryRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
class ItemCategory
{
    /**
     * @ORM\Id
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=UuidGenerator::class)
     * @Groups({"read_category", "write_menu_item", "read_menu_item"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=150)
     * @Groups({"read_category", "write_category", "read_menu_item"})
     * @Assert\NotBlank()
     */
    private $categoryName;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     * @Groups({"read_category", "write_category"})
     */
    private $sortOrder;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"read_category"})
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    /**
     * @ORM\ManyToMany(targetEntity=MenuItem::class, inversedBy="itemCategories")
     */
    private $menuItem;

    public function __construct()
    {
        $this->menuItem = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getCategoryName(): ?string
    {
        return $this->categoryName;
    }

    public function setCategoryName(string $categoryName): self
    {
        $this->categoryName = $categoryName;

        return $this;
    }

    public function getSortOrder(): ?int
    {
        return $this->sortOrder;
    }

    public function setSortOrder(?int $sortOrder): self
    {
        $this->sortOrder = $sortOrder;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return Collection|MenuItem[]
     */
    public function getMenuItem(): Collection
    {
        return $this->menuItem;
    }

    public function addMenuItem(MenuItem $menuItem): self
    {
        if (!$this->menuItem->contains($menuItem)) {
            $this->menuItem[] = $menuItem;
        }

        return $this;
    }

    public function removeMenuItem(MenuItem $menuItem): self
    {
        $this->menuItem->removeElement($menuItem);

        return $this;
    }

    /**
     * @ORM\PrePersist
     */
    public function prePersist()
    {
        $this->createdAt = new \DateTimeImmutable();
        $this->updatedAt = new \DateTimeImmutable();
    }

    /**
     * @ORM\PreUpdate
     */
    public function preUpdate()
    {
        $this->updatedAt = new \DateTimeImmutable();
    }
}
