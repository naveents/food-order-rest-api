<?php
namespace App\FoodSaas\Domain\Model;

use ApiPlatform\Core\Annotation\ApiResource;
use App\FoodSaas\Application\Http\UploadItemImageAction;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity()
 * @Vich\Uploadable()
 * @ApiResource(
 *     collectionOperations={
 *          "get",
 *          "post"={
 *              "method"="POST",
 *              "path"="/item_images",
 *              "controller"=UploadItemImageAction::class,
 *              "defaults"={"_api_receive"=false}
 *          }
 *     },
 *     itemOperations={"get"}
 * )
 */
class ItemImage
{

    const IMAGE_DIR = '/media/';
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Vich\UploadableField(mapping="item_image", fileNameProperty="url")
     */
    private $file;

    /**
     * @ORM\Column(nullable=true)
     * @Groups({"menu:items_list", "read_menu_item"})
     */
    private $url;

    /**
     * @ORM\OneToOne(targetEntity=MenuItem::class, mappedBy="itemImage")
     */
    private $menuItem;

    public function getId()
    {
        return $this->id;
    }

    public function getFile()
    {
        return $this->file;
    }

    public function setFile($file): void
    {
        $this->file = $file;
    }

    public function getUrl()
    {
        return self::IMAGE_DIR.$this->url;
    }

    public function setUrl($url): void
    {
        $this->url = $url;
    }

    public function getMenuItem(): ?MenuItem
    {
        return $this->menuItem;
    }

    public function setMenuItem(?MenuItem $menuItem): self
    {
        // unset the owning side of the relation if necessary
        if ($menuItem === null && $this->menuItem !== null) {
            $this->menuItem->setItemImage(null);
        }

        // set the owning side of the relation if necessary
        if ($menuItem !== null && $menuItem->getItemImage() !== $this) {
            $menuItem->setItemImage($this);
        }

        $this->menuItem = $menuItem;
    }


}