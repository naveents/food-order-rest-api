<?php
namespace App\FoodSaas\Application\Http;

use ApiPlatform\Core\Validator\Exception\ValidationException;
use App\FoodSaas\Domain\Form\ImageType;
use App\FoodSaas\Domain\Model\ItemImage;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;

class UploadItemImageAction extends AbstractController
{
    /**
     * @var FormFactoryInterface
     */
    protected $formFactory;

    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    public function __construct(
        FormFactoryInterface $formFactory,
        EntityManagerInterface $entityManager
    )
    {
        $this->formFactory = $formFactory;
        $this->entityManager = $entityManager;
    }

    public function __invoke(Request $request)
    {
        $image = new ItemImage();
        // validate form
        $form = $this->formFactory->create(ImageType::class, $image);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            // save image entity
            $this->entityManager->persist($image);
            $this->entityManager->flush();
            $image->setFile(null);
            return $image;
        }
        throw new ValidationException(
            'Invalid Image Uploaded!'
        );
    }
}